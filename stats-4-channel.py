#!/usr/bin/python3

import click, random, csv, os, pickle, time, sys
import google.oauth2.credentials
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from pathlib import Path
from collections import Counter

SCOPES = ['https://www.googleapis.com/auth/youtube.force-ssl']
API_SERVICE_NAME = 'youtube'
API_VERSION = 'v3'
HOME = str(Path.home())
CACHE_CREDENTIALS_DIR = HOME + "/.ytapi/"
CACHE_CREDENTIALS_FILE = CACHE_CREDENTIALS_DIR + "token.pickle"

def get_authenticated_service(CLIENT_SECRETS_FILE):
    credentials = None
    if not os.path.exists(CACHE_CREDENTIALS_DIR):
      os.makedirs(CACHE_CREDENTIALS_DIR)
    if os.path.exists(CACHE_CREDENTIALS_FILE):
        with open(CACHE_CREDENTIALS_FILE, 'rb') as token:
            credentials = pickle.load(token)
    #  Check if the credentials are invalid or do not exist
    if not credentials or not credentials.valid:
        # Check if the credentials have expired
        if credentials and credentials.expired and credentials.refresh_token:
            credentials.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                CLIENT_SECRETS_FILE, SCOPES)
            credentials = flow.run_console()
        # Save the credentials for the next run
        with open(CACHE_CREDENTIALS_FILE, 'wb') as token:
            pickle.dump(credentials, token)
    return build(API_SERVICE_NAME, API_VERSION, credentials = credentials)
 
def get_playlists_channel(service, **kwargs):
    playlists = []
    results = service.playlists().list(**kwargs).execute()
    while results:
        for item in results['items']:
           if item['status']['privacyStatus'] == "public":
             playlist_title=item['snippet']['title']
             playlist_id=item['id']
             if playlist_id == "PLn6POgpklwWoaHbxXgb5Tukdbj6jQ-IIz" or playlist_id == "PLn6POgpklwWrDLisy2QoSZgoN1ZLQvgFV":
               continue
             playlists.append([playlist_title,playlist_id])
        # Check if another page exists
        if 'nextPageToken' in results:
            kwargs['pageToken'] = results['nextPageToken']
            results = service.playlists().list(**kwargs).execute()
        else:
            break
    return playlists

def get_video_playlist(service, **kwargs):
    videos = []
    results = service.playlistItems().list(**kwargs).execute()
    while results:
        for item in results['items']:
           title=item['snippet']['title']
           videoId=item['snippet']['resourceId']['videoId']
           videos.append([title,videoId])
        # Check if another page exists
        if 'nextPageToken' in results:
            kwargs['pageToken'] = results['nextPageToken']
            results = service.playlistItems().list(**kwargs).execute()
        else:
            break
    return videos

def get_video_comments(service, **kwargs):
    authors = []
    # request google api
    results = service.commentThreads().list(**kwargs).execute()
    while results:
        # keep comments and authors
        for item in results['items']:
            comment = item['snippet']['topLevelComment']['snippet']['textDisplay']
            author = item['snippet']['topLevelComment']['snippet']['authorDisplayName']
            authors.append(author)
        # Check if another page exists
        if 'nextPageToken' in results:
            kwargs['pageToken'] = results['nextPageToken']
            results = service.commentThreads().list(**kwargs).execute()
        else:
            break
    return authors

def get_channel_stats(service, **kwargs):
    channelStats = {}
    results = service.channels().list(**kwargs).execute()
    for item in results['items']:
      channelStats['subscribers'] = item['statistics']['subscriberCount']
      channelStats['views'] = item['statistics']['viewCount']
      channelStats['videos'] = item['statistics']['videoCount']
    return channelStats

@click.command()
@click.option('--subscribers', '-s', is_flag=True, help="Count of subscribers")
@click.option('--views', '-v', is_flag=True, help="Count of views")
@click.option('--videos', '-m', is_flag=True, help="Count of videos")
@click.option('--cred', '-c', default="" ,help="Credentials file (json format) to youtube api")
def cli(cred,subscribers,videos,views):
  """
  This python script list top comments for a youtube channel and some other datas.
  """
  os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1' #warning it is insecure exchange
  service = get_authenticated_service(cred)
  keyword = input('Enter a channel ID : ')
  list_videos = []
  result=get_channel_stats(service, part='statistics',mine=True)
  if videos: print(result['videos'])
  if subscribers: print(result['subscribers'])
  if views: print(result['views'])

  result = get_playlists_channel(service, part="snippet,status", channelId=keyword)
  all_authors = []
  for play in result:
    all = get_video_playlist(service, part="snippet,contentDetails", playlistId=play[1])
    for row in all:
      list_videos.append(row[1])
  all_comments = []
  for video in list_videos:
    for vid_comments in get_video_comments(service, part='snippet', videoId=video, textFormat='plainText'):
      all_comments.append(vid_comments)

  top = Counter(all_comments).most_common(20)
  for twenty in top:
    print(twenty[0] + " -> " + str(twenty[1]))
if __name__ == '__main__':
  cli()

