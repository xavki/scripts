import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

url = input("Enter Youtube Channel URL : ")
driver = webdriver.Chrome()

driver.get(url+"/videos")
ht=driver.execute_script("return document.documentElement.scrollHeight;")
while True:
    prev_ht=driver.execute_script("return document.documentElement.scrollHeight;")
    driver.execute_script("window.scrollTo(0, document.documentElement.scrollHeight);")
    time.sleep(2)
    ht=driver.execute_script("return document.documentElement.scrollHeight;")
    if prev_ht==ht:
        break

links=driver.find_elements_by_xpath('//*[@id="video-title"]')
for link in links:
    print(link.get_attribute("href"))
