#!/usr/bin/python3

import click, random, csv, os, pickle, time, sys
import google.oauth2.credentials
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from pathlib import Path

SCOPES = ['https://www.googleapis.com/auth/youtube.force-ssl']
#SCOPES = ['https://www.googleapis.com/auth/youtube.readonly']
API_SERVICE_NAME = 'youtube'
API_VERSION = 'v3'
HOME = str(Path.home())
CACHE_CREDENTIALS_DIR = HOME + "/.ytapi/"
CACHE_CREDENTIALS_FILE = CACHE_CREDENTIALS_DIR + "token.pickle"

def get_authenticated_service(CLIENT_SECRETS_FILE):
    credentials = None

    # check if creds path exist
    if not os.path.exists(CACHE_CREDENTIALS_DIR):
      os.makedirs(CACHE_CREDENTIALS_DIR)

    # check if creds file exist
    if os.path.exists(CACHE_CREDENTIALS_FILE):
        with open(CACHE_CREDENTIALS_FILE, 'rb') as token:
            credentials = pickle.load(token)

    #  Check if the credentials are invalid or do not exist
    if not credentials or not credentials.valid:

        # Check if the credentials have expired
        if credentials and credentials.expired and credentials.refresh_token:
            credentials.refresh(Request())
        else:
						# else auth by google
            flow = InstalledAppFlow.from_client_secrets_file(
                CLIENT_SECRETS_FILE, SCOPES)
            credentials = flow.run_console()
        # Save the credentials into the pickle for the next run
        with open(CACHE_CREDENTIALS_FILE, 'wb') as token:
            pickle.dump(credentials, token)
    return build(API_SERVICE_NAME, API_VERSION, credentials = credentials)
 
def get_video_comments(word, service, **kwargs):
    comments = []
    # request google api
    results = service.commentThreads().list(**kwargs).execute()
    while results:
        # keep comments and authors
        for item in results['items']:
            comment = item['snippet']['topLevelComment']['snippet']['textDisplay']
            author = item['snippet']['topLevelComment']['snippet']['authorDisplayName']
            if word.casefold() in comment.casefold():
              comments.append([author,comment])
        # Check if another page exists
        if 'nextPageToken' in results:
            kwargs['pageToken'] = results['nextPageToken']
            results = service.commentThreads().list(**kwargs).execute()
        else:
            break
    return comments
 
def write_to_file(comments,output_file):
    # open a file to wrtie comments
    with open(output_file, 'w') as comments_file:
      for row in comments:
        comments_file.write("{} || {}\n".format(row[0],row[1]))

def random_selection(list_comments):
    # keep users a use a set to remove duplicate users
    list_comments = list(( i[0]for i in list_comments ))
    # start random selection
    max = len(list_comments) - 1
    rand = random.randint(0,max)
    return list_comments[rand]

@click.command()
@click.option('--output', '-o', help="Enter your output file")
@click.option('--word', '-w', default="" ,help="Add a filter")
@click.option('--cred', '-c', default="" ,help="Credentials file (json format) to youtube api")
@click.option('--rand', '-r', is_flag=True, help="Select a random comment in the list")
def cli(output,word,cred,rand):
  """
  This python script list all comments for a youtube video.
  And you can add argument as filter and output file.
  You must have google api credentials : https://console.developers.google.com
  The script use credentials for the first connection and use cache after (with token.pickle).
  """
  os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1' 	#warning it is insecure exchange
  service = get_authenticated_service(cred) 				# google authentication for the request
  keyword = input('Enter a video ID : ') 						# prompt video id
  result=get_video_comments(word, service, part='snippet', videoId=keyword, textFormat='plainText') # capture result
  print(
  """
####################################################
****************************************************

    Déjà {} commentaires


****************************************************
####################################################
    """.format(len(result))
  )
  
  # random selection
  if rand:
    select = random_selection(result)
    sec = 20

    # final countdown
    while True:
      print("""
      {} seconds...
      """.format(sec))
      sec -= 1
      time.sleep(1)
      if sec == 0:
        break

    # then the result
    print(
    """
####################################################
****************************************************

    THE WINNER IS...  >>> {}

****************************************************
####################################################
    """.format(select)
    )
    sys.exit()
  
  # if output to file
  for row in result:
    print("{} || {}\n".format(row[0],row[1]))
  if output:
    write_to_file(result,output)

if __name__ == '__main__':
  cli()

